require "itunes-search-api"

module Jekyll
	module ItunesLinkConverter

		def app_name(app_id)
			app(app_id)["trackName"]
		end

		def app_image(app_id)
			app(app_id)["artworkUrl100"]
		end

		def app_link(app_id)
			app(app_id)["trackViewUrl"]
		end

		def app(app_id)
			app = ITunesSearchAPI.lookup(:id => app_id)
			raise "App with store id #{app_id} does not exist" unless app
			app
		end
	end
end

Liquid::Template.register_filter(Jekyll::ItunesLinkConverter)
