---
layout: post
title: "Explain your job like I'm five"
description: ""
category:
tags: []
---

Camille, my girlfriend has been volunteering for a while at a place called “[la maison des enfants](http://www.lamaisondesenfants.qc.ca)”. I went there once a few months ago and from what I saw and heard, these people are working hard to make sure children of the neighbourhood have a place they can go to to be heard and have fun. Last Saturday, they were looking for people willing to talk about their job and answer their questions, so that’s where I spent my Saturday morning.

We had a hard time getting children interested in a guy doing “websites and applications for your phone”. In the same room there was a paramedic with his tools and uniform so it was really hard to compete. Fortunately, we manage to find the magic word: it’s not about phones, it’s about tablets. Kids at this age usually don’t have iPhone, but they play games and watch videos on the family’s tablet.

This wasn’t the first time I’ve been presenting something. If you follow me on twitter you may have heard about CocoaHeads Montreal, a user group where I’ve been doing [a few presentations](https://speakerdeck.com/romainpouclet). As it turns out, speaking to a bunch of five to eight years old is **way** more stressful than talking to developers while drinking beers. Kids talk with this unfiltered honesty and beautiful naivety that turned me speechless a couple of times.

Luckily for me, every professional was paired with a volunteer, in my case it was Camille. She rightfully stepped in a few times and helped the kids ask their questions. I let them play with [a cool application](https://itunes.apple.com/us/app/scholastic-book-fairs/id699636487?mt=8) I worked on last year and opened Xcode to show them the gibberish behind the apps they use. One of the kid seemed really proud to show me he understood that the more buttons your application had, the more code you had to write.

Overall, this was a really neat experience that I’m looking forward to doing again next year. I’ll probably ask to be with older kids though, I’m pretty sure it’ll be more interesting for them. Hopefully, I’ll be able to convince a few that working as a developper is way cooler than working as a fireman or a paramedic!
