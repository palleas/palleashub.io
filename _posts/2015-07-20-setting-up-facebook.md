---
layout: post
title: "Setting up Facebook in your iOS application"
description: ""
category:
tags: []
---

I have mixed feelings about Facebook. I’ve often considered closing my account but a few days ago, I created an event to invite my friends to celebrate my 28th birthday so ¯\\_(ツ)_/¯. Setting up Facebook in an iOS application is super easy if you follow the steps provided in the official documentation. Let’s see how you can improve those instructions and make your setup a little better. 

# Adding the Facebook SDK as a dependency.

This is what the official documentation wants you to do:
 
	Download the SDK and install the package by following the wizard. The default install location is ~/Documents/FacebookSDK.

There is no perfect solution when it comes to dependency management but in this case, Cocoapods is more than fit for the job, using the `FBSDKCoreKit` and `FBSDKLoginKit` pods. If you’re new to Cocoapods, here is a link for you. Now that we’ve added the pod to our project, let’s see how we can managed the AppId and URL schemes part.

# Identifying your Facebook application using this one weird trick

In the “getting started” tutorial, Facebook tells you to hardcode a few things in your application’s Plist file. It’s not a bad practice per se but here is a nicer one for you: using configuration files.

Start by creating a **“MyApplication”** configuration file. You can find the template for this type of file in the “Other” section of the new file panel. 

You can only have one main configuration file per target per configuration. To make sure we don’t break the setup created by Cocoapods to manage the dependencies, we’ll first need to include the configuration file it created during the `pod install` process:

```
#include “Pods/Target Support Files/Pods-MyApplication/Pods-MyApplication.debug.xcconfig”
```

This configuration file will be used to store the application id in a dedicated user-defined build setting:

```
FACEBOOK_APP_ID = 1234567890876543
```

The nice thing about all of the build settings is they’re available in your property list file. that’s why in you open this file using a text editor, you’ll see `com.perfectly-cooked.$(PRODUCT_NAME:rfc1034identifier)` in the `CFBundleIdentifier`. We’ll use the same approach for the application id and the URL scheme you need to configure: instead of **“1234567890876543”** we’ll use **$(FACEBOOK_APP_ID)** and instead of **“fb1234567890876543”**, we’ll use **“fb$(FACEBOOK_APP_ID)”**.

Let’s be clear: it’s not a more secure way to manage your Facebook application id, simply a more convenient way to put everything in one place. 
