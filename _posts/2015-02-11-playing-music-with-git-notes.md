---
layout: post
title: "Playing (music) with git notes"
description: ""
category:
tags: []
---

Did you know that you can add notes to a git commit? Of course you do, because you’ve read [Scott Chacon and Ben Straub book](http://git-scm.com/book/en/v2) that tells you everything about git*. The cool thing about git notes is that they are not pushed with the commit (unless you actually want to) and that they can be “namespaced”. You’ll find everything you need about notes [on this page](http://git-scm.com/book/en/v2).

![Calvin Russell](/assets/2015/nowplaying-calvinrussell.png)

Like a lot of people, I enjoy listening to music when <del>my girlfriend is watching parenthood</del> I work, usually with [Spotify](http://spotify.com). Currently playing while I write this short post is [Calvin Russell’s Petit gars](http://open.spotify.com/track/7wOUW7UeEMQtcNFiKxRqLI). I know that because an AppleScript told me.

![Now playing with AppleScript](/assets/2015/nowplaying-script.png)

I realized that I completely forgot to re-enable scrobbling to [lastfm](http://www.last.fm/user/palleas) when I clean-installed Yosemite and it made me think of something. How about linking what I’m working on to what I’m listening to?

The first step you need to do is to store somewhere the following Applescript file (in my case, that’s ~/Scripts/Spotify-nowplaying.scpt):

	tell application "Spotify"
		artist of current track & " - " & name of current track & " (" & spotify url of current track & ")"
	end tell

Because I don’t like hardcoding anything, I’ve stored the fullpath to this script into a [git configuration key](http://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration), like this:

    $ git config --global --add nowplaying.path "$HOME/Scripts/Spotify-nowplaying.scpt"

Finally, I’ve added the following lines into a [git post-commit hook](http://git-scm.com/book/be/v2/Customizing-Git-Git-Hooks) (don’t forget to `chmod +x .git/hooks/post-commit` or your hook won’t be invoked at all):

	#!/bin/sh
	path=$(git config nowplaying.path)
	nowplaying=`osascript $path`
	git notes --ref=nowplaying add -m ":musical_note: - $nowplaying"

Now, every time I’m committing something I’ll actually have a nice note showing information about the current track I was listening to.

**Known issue:** the script will fail if Spotify isn’t running.

**Workaround:** listen to music.

![Git log with music](/assets/2015/nowplaying-result.png)

(* to be honest I’m not sure git notes are actually mentioned)
