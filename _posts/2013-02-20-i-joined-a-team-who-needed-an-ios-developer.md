---
layout: post
title: "I joined a team who needed an iOS developer"
description: ""
category:
tags: []
---

I started working with a couple of friends on a project they've been working on for a while. The iOS developer they were working with decided to quit for personal reasons, so they asked me to join their team. It's never easy to take over after another developer, of course you wouldn't have done it the way he did it. This time I figured it would be a good exercise for me to write some kind of before/after post series.

# Day one : I got the source code… in a zip archive

I was really happy when I found a .git folder, albeit a bit worried to receive it in a zip archive, and I think I was right.

![git log](https://dl.dropbox.com/u/752727/calepin-images/1-gitlog.png)

One of the cool things Xcode 4 brought was native support of git. In fact, when you create a new project, you have to explicitly tell Xcode you don't want to use git.
Here is a quick reminder : git is **awesome**, branches are cheap and everything works locally so don't be afraid to use it, even (especially?) if you're working alone. Reading this project commit log could have helped me understanding my predecessor's code, as he forgot to add comments, a README file or any kind of documentation.

# Day two : bootstrapping sonar

I figured it would be a great opportunity to give a try to [a sonar plugin for Objective-C](https://github.com/fhelg/sonar-objective-c/) I ran into a couple of days ago. I've never actually liked installing heavy, complicated tools like sonar, using heavier, more complicated tools like maven but I may have been lucky this time because it worked on my second try.

* First, I initialized an empty vagrant box. As I'm not much of a devops, I figured it'd be safer.
* Then, I installed sonar following the screencast on sonar's official website. Installed maven, tried to build the plugin and… it failed. Nothing a reinstallation of jdk-6 couldn't fix.
* Finally, I started sonar, added a [pom.xml](https://github.com/fhelg/sample-sonar-objective-c/blob/master/pom.xml) file and ran "mvn sonar:sonar", just like [the sample project said](https://github.com/fhelg/sample-sonar-objective-c/blob/master/README.md).

This is what sonar taught me :

* No violation
* 3% code duplication
* 14273 LOC in 23577 lines
* 102 files in 3 directories

# Now what

Now that I have a couple of metrics I can watch, I'm going to start cleaning the code and install the other fancy tools I like to make sure my participation goes smoothly. I'll make sure I write a second post about how it went, hoping it'll be helpful for somebody.

(Thanks to Benjamin, one of the nice guys at [Alpixel](http://alpixel.fr) who accepted to proofread my rusty english.)
