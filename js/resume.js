;jQuery(function ($) {
	$(".long_description").hide();

	$(".read_more_link")
		.on("click", function(event) {
			var expandable = $(this).data("expandable");
			$("#" + expandable).slideToggle(400);
		})
	;
});